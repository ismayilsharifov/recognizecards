package cardsrecognizer;

import java.awt.image.BufferedImage;
import java.io.File;

public class Utility {
    protected static final int[] topLeftCornerX = {149, 219, 289, 364, 439}; //Indexes of the columns of the upper left corner of the image matrix fragment
    protected static final int topLeftCornerY[] = {589, 617}; //Indexes of the rows of the upper left corner of the image matrix fragment
    protected static final int rankSize[] = {41, 31}; //the size of the map significance image fragment
    protected static final int suitSize[] = {23, 21}; //card suit image fragment size

    public static boolean isImage(File file) {
        if (file != null) {
            String filename = file.getName();
            String extension = null;
            int separator = filename.lastIndexOf('.');
            if (separator > 0 && separator < filename.length() - 1) {
                extension = filename.substring(separator + 1).toLowerCase();
            }
            if (!file.isDirectory() && extension != null && extension.equalsIgnoreCase("png"))
                return true;
        }
        return false;
    }

    public static String getImageAsString(BufferedImage symbol) {
        short whiteBg = -1;
        StringBuilder binaryString = new StringBuilder();
        for (short y = 1; y < symbol.getHeight(); y++) {
            for (short x = 1; x < symbol.getWidth(); x++) {
                int rgb = symbol.getRGB(x, y);
                binaryString.append(rgb == whiteBg ? " " : "*");
            }
        }
        return binaryString.toString();
    }

    public static BufferedImage getBinaryImage(BufferedImage img, char type) {
        BufferedImage new_image = img;
        for (int i = 0; i < new_image.getWidth(); i++) {
            for (int j = 0; j < new_image.getHeight(); j++) {
                if (type == 's') {
                    if (new_image.getRGB(i, j) == -8882056) {
                        new_image.setRGB(i, j, -1);
                    } else {
                        new_image.setRGB(i, j, 0);
                    }
                } else if (type == 't') {
                    if (new_image.getRGB(i, j) != -1) {
                        new_image.setRGB(i, j, 0);
                    }
                }

            }
        }
        return new_image;
    }
}
