package cardsrecognizer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static cardsrecognizer.Utility.*;

public class Recognizer {
    private BufferedImage img;
    public Recognizer(BufferedImage img) {
        this.img = img;
    }

    public String getRankOrSuit(int number, char type) {
        String rankSuit = "";
        boolean isFullFourthOrFifthCard = false;
        BufferedImage img1 = null;
        if (type == 'r') {
            img1 = img.getSubimage(topLeftCornerX[number], topLeftCornerY[0], rankSize[0], rankSize[1]);
        } else if (type == 's') {
            img1 = img.getSubimage(topLeftCornerX[number], topLeftCornerY[1], suitSize[0], suitSize[1]);
        }
        boolean isGray = false;
        if (img1.getRGB(img1.getWidth() - 1, img1.getHeight() - 1) == -8882056) {//if the last pixel is a gray pixel need to convert to binary image
            isGray = true;
            img1 = getBinaryImage(img1, 's');
        }
        String originalString = getImageAsString(img1); //convert the rank/suit of the given card to a binary string

        if (number > 2) { //if it is the place of the fourth or fifth card get the pixel value and verify is there a fourth or fifth card
            Color c = new Color(img1.getRGB(img1.getWidth() - 1, img1.getHeight() - 1));
            isFullFourthOrFifthCard = isFourthOrFifthCard(c.getRed(), c.getGreen(), c.getRed());
        }

        if (number < 3 || (number > 2 && isFullFourthOrFifthCard == true)) {
            File dir = null;
            if (type == 'r') {
                dir = new File("combinations\\ranks"); //13 rank
            } else if (type == 's') {
                dir = new File("combinations\\suits"); //4 suit
            }
            int min = 1000000;
            for (final File imgFile : dir.listFiles()) {
                BufferedImage target = null;
                try {
                    target = ImageIO.read(imgFile);
                    if (isGray == true) {
                        target = getBinaryImage(target, 't');
                    }

                } catch (IOException e) {
                    System.out.println("The image " + imgFile.getName().replaceFirst("[.][^.]+$", "") + " cannot be opened!");
                }
                String targetString = getImageAsString(target);
                int levenshtein = levenshtein(originalString, targetString, 1000); //Levenshtein distance
                if (levenshtein < min) {
                    min = levenshtein;
                    rankSuit = imgFile.getName().replaceFirst("[.][^.]+$", "");
                }
            }
        }
        return rankSuit;
    }

    public boolean isFourthOrFifthCard(int r, int g, int b) { //Is there a fourth or fifth card, if it is a white or gray pixel there is a card.
        if ((r == 255 && g == 255 && b == 255) || (r == 120 && g == 120 && b == 120)) {
            return true;
        } else {
            return false;
        }
    }

    public int levenshtein(String source, String target, int threshold) {
        int N1 = source.length();
        int N2 = target.length();
        int p[] = new int[N1 + 1];
        int d[] = new int[N1 + 1];
        int temp[];

        final int boundary = Math.min(N1, threshold) + 1;
        for (int i = 0; i < boundary; i++) {
            p[i] = i;
        }
        Arrays.fill(p, boundary, p.length, Integer.MAX_VALUE);
        Arrays.fill(d, Integer.MAX_VALUE);

        for (int j = 1; j <= N2; j++) {
            char t_j = target.charAt(j - 1);
            d[0] = j;

            int min = Math.max(1, j - threshold);
            int max = (j > Integer.MAX_VALUE - threshold) ? N1 : Math.min(N1, j + threshold);

            if (min > max) {
                return -1;
            }
            if (min > 1) {
                d[min - 1] = Integer.MAX_VALUE;
            }
            for (int i = min; i <= max; i++) {
                if (source.charAt(i - 1) == t_j) {
                    d[i] = p[i - 1];
                } else {
                    d[i] = 1 + Math.min(Math.min(d[i - 1], p[i]), p[i - 1]);
                }
            }
            temp = p;
            p = d;
            d = temp;
        }
        if (p[N1] <= threshold) {
            return p[N1];
        }
        return -1;
    }
}
