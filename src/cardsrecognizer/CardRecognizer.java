package cardsrecognizer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static cardsrecognizer.Utility.isImage;

public class CardRecognizer {
    public static void main(String[] args) {
        Recognizer recognizer;
        File dir = new File(args[0]);
        for (final File imgFile : dir.listFiles()) {
            if (isImage(imgFile)) {
                BufferedImage img = null;
                try {
                    img = ImageIO.read(imgFile);
                } catch (IOException e) {
                    System.out.println("The image " + imgFile.getName() + " cannot be opened!");
                }
                String results = "";
                recognizer = new Recognizer(img);
                for (int i = 0; i < 5; i++) {
                    results = results + recognizer.getRankOrSuit(i, 'r');
                    results = results + recognizer.getRankOrSuit(i, 's');
                }
                System.out.println(imgFile.getName() + " - " + results);
            }
        }
    }
}
