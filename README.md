To start the program RecognizeCards.jar for recognizing cards, in the CMD console you need to write the command run.bat "images_path", where the images_path parameter is the path to the folder with images. For example,
`run.bat "D:\Downloads\RecognizeCards\images" `
The start.bat file must be in the same folder as RecognizeCards.jar and with the combinations folder, where template images of all ranks and suits are located.

Task and requirements:

- Given a lot of pictures
- It is necessary to write a Java program that recognizes which cards are on the table (only in the center of the picture). 
- Testing of the program will be carried out on similar pictures that are not in the original set
- Errors in recognition are allowed no more than 3% of the total number of recognized cards
- You can not use ready-made libraries for text recognition. You need to write your own card recognition algorithm
- It should not take more than 1 second to recognize one file
- The source code for solving the problem should not be longer than 500 lines with normal formatting
- The program must be provided in a form ready to run on a Windows desktop. The run.bat file takes as a parameter the path to the folder with pictures. The result is printed to the console in the form "file name - maps" for all files in the folder
- The program must be provided with source files
- The source files must contain all the code that was used to solve the problem.
